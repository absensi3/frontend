import React from "react";

function Checkbox({
  methods,
  label,
  trueValue = true,
  falseValue = false,
  ...props
}) {
  return (
    <div className={props.className}>
      <label className="inline-flex items-center">
        <input
          {...methods.register(props.name)}
          {...props}
          type="checkbox"
          defaultChecked={props.value === trueValue}
          className="mr-1"
        />
        {label}
      </label>
      <div className={props.error ? "text-red-600 text-sm" : "hidden"}>
        {props.error}
      </div>
    </div>
  );
}

export default Checkbox;
