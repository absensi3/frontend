import React, { useCallback, useEffect, useState } from "react";
import adminApi from "../../api/admin";
import Card from "../../components/Card";
import { ReactComponent as Trush } from "../../assets/trash.svg";
import { ReactComponent as Pen } from "../../assets/pen.svg";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logoutAction } from "../../store/admin";
import Dialog from "../../components/Dialog";

function DaftarAdmin({ showSnackbar }) {
  const [admins, setAdmins] = useState([]);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [selected, setSelected] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const deleteAdmin = async () => {
    try {
      const res = await adminApi.delete(selected);
      if (res.status === 200) {
        showSnackbar(res.data.msg);
        setSelected("");
        setDeleteDialog(false);
        loadAdmin();
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    }
  };
  const loadAdmin = useCallback(async () => {
    try {
      const res = await adminApi.getAll();
      if (res.status === 200) {
        setAdmins(res.data.data);
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    }
  }, [dispatch, navigate, showSnackbar]);

  useEffect(() => {
    loadAdmin();
  }, [loadAdmin]);

  return (
    <div>
      <Card>
        <div className="text-lg font-semibold">Daftar Admin</div>
        <table className="table-auto rounded shadow w-full mt-3">
          <thead>
            <tr className="bg-gray-200 text-left">
              <th className="px-3 py-2 rounded-tl">Username</th>
              <th className="px-3 py-2">Email</th>
              <th className="px-3 py-2">Superadmin</th>
              <th className="px-3 py-2 rounded-tr">Aksi</th>
            </tr>
          </thead>
          <tbody>
            {admins.map((admin, idx) => (
              <tr key={idx}>
                <td className="px-3 py-2">{admin.username}</td>
                <td className="px-3 py-2">{admin.email}</td>
                <td className="px-3 py-2">{admin.super.toString()}</td>
                <td className="px-3 py-2 flex gap-2">
                  <button
                    onClick={() => {
                      setSelected(admin.uuid);
                      setDeleteDialog(true);
                    }}
                    className="rounded-full bg-green-400 p-2"
                  >
                    <Trush className="h-5 w-5  fill-gray-200" />
                  </button>
                  <button
                    onClick={() => navigate(`/update-admin/${admin.uuid}`)}
                    className="rounded-full bg-green-400 p-2"
                  >
                    <Pen className="h-5 w-5  fill-gray-200" />
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </Card>
      <Dialog show={deleteDialog} toggle={() => setDeleteDialog(false)}>
        <Card>
          <div>{`Apakah anda yakin untuk menghapus admin ${
            selected && admins.find((admin) => admin.uuid === selected).username
          }`}</div>
          <div className="flex justify-end mt-5">
            <button
              className="bg-red-600 px-4 py-1 rounded text-white"
              onClick={() => deleteAdmin()}
            >
              Ya
            </button>
            <button
              className="ml-5 bg-green-500 px-4 py-1 rounded"
              onClick={() => setDeleteDialog(false)}
            >
              Tidak
            </button>
          </div>
        </Card>
      </Dialog>
    </div>
  );
}

export default DaftarAdmin;
