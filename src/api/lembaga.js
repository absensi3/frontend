import api from ".";

const lembagaApi = {
  getAll: () => {
    return api.get(`/lembaga`);
  },
  add: (data) => {
    return api.post(`/lembaga`, data);
  },
  getOne: (uuid) => {
    return api.get(`/lembaga/${uuid}`);
  },
  update: (data, uuid) => {
    return api.put(`/lembaga/${uuid}`, data);
  },
  delete: (uuid) => {
    return api.delete(`/lembaga/${uuid}`);
  },
};

export default lembagaApi;
