import React from "react";

function Radio({ methods, name, label, className, options }) {
  return (
    <div className={className}>
      <div className="relative">
        <label
          className={`absolute -top-[7px] left-2 bg-white text-xs ${
            methods.formState.errors[name] ? "text-red-600" : ""
          }`}
        >
          {label}
        </label>
        <div
          className={`flex flex-col p-2 rounded border-[1px] ${
            methods.formState.errors[name]
              ? "border-red-600"
              : "border-gray-500"
          }`}
        >
          {options.map((option, idx) => {
            return (
              <label
                key={idx}
                className={`inline-flex items-center ${
                  methods.formState.errors[name] ? "text-red-600" : ""
                }`}
              >
                <input
                  {...methods.register(name)}
                  type="radio"
                  value={option.value}
                  onClick={() => methods.setValue(label, option.value)}
                  className="mr-1"
                />
                {option.label}
              </label>
            );
          })}
        </div>
      </div>
      {methods.formState.errors[name] && (
        <p className="text-red-600 text-xs pl-2">
          {methods.formState.errors[name].message}
        </p>
      )}
    </div>
  );
}

export default Radio;
