import React, { useEffect, useState } from "react";

function Dialog(props) {
  const [show, setShow] = useState(props.show);
  const [scale, setScale] = useState(props.show);
  useEffect(() => {
    setTimeout(
      () => {
        setShow(props.show);
      },
      props.show ? 0 : 100
    );
    setTimeout(
      () => {
        setScale(props.show);
      },
      props.show ? 100 : 0
    );
  }, [props.show]);

  return (
    <div
      className={`fixed inset-0 bg-black overflow-x-hidden overflow-y-hidden h-screen flex justify-center items-center  transition-all duration-300 z-50 ${
        show ? "bg-opacity-30 visible" : "bg-opacity-0 invisible"
      }`}
      onClick={() => props.toggle(false)}
    >
      <div
        onClick={(e) => e.stopPropagation()}
        className="relative mx-auto w-auto"
      >
        <div
          className={`bg-white w-full bg-opacity-100 transition-all duration-300 rounded shadow-2xl ${
            show ? "opacity-100" : "opacity-0"
          } ${scale ? "scale-100" : "scale-0"}`}
        >
          {props.children}
        </div>
      </div>
    </div>
  );
}

export default Dialog;
