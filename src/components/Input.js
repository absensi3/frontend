import React from "react";

function Input({ methods, label, prefix, suffix, className, ...props }) {
  return (
    <div className={className}>
      <div className="flex relative">
        {prefix && (
          <div
            className={`flex items-center justify-center border-y-[1px] border-l-[1px] rounded-l-md ${
              methods.formState.errors[props.name]
                ? "border-red-600 fill-red-600"
                : "border-gray-500 fill-black"
            }`}
          >
            {prefix}
          </div>
        )}
        <input
          {...methods.register(props.name)}
          {...props}
          id={props.name}
          className={`border-[1px] peer ${
            methods.formState.errors[props.name]
              ? "border-red-600 text-red-600"
              : "border-gray-500"
          } focus:${
            methods.formState.errors[props.name]
              ? "border-red-600"
              : "border-green-600"
          } focus:outline-none flex-1 px-2 py-1 ${
            prefix ? "" : "rounded-l-md"
          } ${suffix ? "" : "rounded-r-md"}`}
        />
        <label
          htmlFor={props.name}
          className={`absolute peer-placeholder-shown:top-1 transition-all duration-500 peer-placeholder-shown:opacity-0 cursor-text peer-focus:opacity-100 peer-focus:-top-[7px] -top-[7px] peer-focus:text-xs peer-focus:bg-white bg-white ${
            methods.getValues(props.name) && "text-xs"
          } ${
            prefix
              ? "left-[38px] peer-focus:left-[38px] "
              : "left-[9px] peer-focus:left-[9px]"
          } ${methods.formState.errors[props.name] ? "text-red-600" : ""}`}
        >
          {label}
        </label>
        {suffix && (
          <div
            className={`"flex items-center justify-center border-y-[1px] border-r-[1px] rounded-r-md m-1" ${
              methods.formState.errors[props.name]
                ? "border-red-600 fill-red-600"
                : "border-gray-500 fill-black"
            }`}
          >
            {suffix}
          </div>
        )}
      </div>
      {methods.formState.errors[props.name] && (
        <p className="text-red-600 text-xs pl-2">
          {methods.formState.errors[props.name].message}
        </p>
      )}
    </div>
  );
}

export default Input;
