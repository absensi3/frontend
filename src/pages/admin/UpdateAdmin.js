import { yupResolver } from "@hookform/resolvers/yup";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import Card from "../../components/Card";
import Checkbox from "../../components/Checkbox";
import Input from "../../components/Input";
import Loading from "../../assets/loading.png";
import adminApi from "../../api/admin";
import { useDispatch } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { logoutAction } from "../../store/admin";

const UpdateAdmin = ({ showSnackbar }) => {
  const [loading, setLoading] = useState(false);
  const methods = useForm({
    mode: "all",
    resolver: yupResolver(
      yup.object().shape({
        username: yup
          .string()
          .required("Username tidak boleh kosong")
          .min(3, "Username minimal 3 karakter"),
        password: yup
          .string()
          .notRequired()
          .min(8, "Password minimal 8 karakter")
          .nullable()
          .transform((v) => (!!v ? v : null)),
        email: yup
          .string()
          .required("Password tidak boleh kosong")
          .email("Email tidak valid"),
        super: yup.boolean(),
      })
    ),
  });
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const updateAdmin = async (data) => {
    setLoading(true);
    try {
      const res = await adminApi.updateProfile(data, param["uuid"]);
      if (res.status === 200) {
        showSnackbar(res.data.msg);
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    } finally {
      methods.reset();
      setLoading(false);
      navigate("/");
    }
  };
  const param = useParams();
  useEffect(() => {
    const loadAdmin = async () => {
      try {
        const res = await adminApi.getOne(param["uuid"]);
        if (res.status === 200) {
          methods.reset(res.data.data);
        }
      } catch (err) {
        if (err.hasOwnProperty("response")) {
          if (err.response.status === 401) {
            dispatch(logoutAction());
            navigate("/login");
          }
          showSnackbar(err.response.data.msg);
        }
        console.log(err);
      }
    };
    loadAdmin();
  }, [dispatch, methods, navigate, param, showSnackbar]);

  return (
    <div>
      <Card>
        <div className="text-xl font-bold">Update Admin</div>
        <form onSubmit={methods.handleSubmit(updateAdmin)}>
          <Input
            label="Username"
            name="username"
            methods={methods}
            placeholder="Username"
            className="mt-3"
          />
          <Input
            label="Password"
            name="password"
            methods={methods}
            placeholder="Password"
            className="mt-3"
          />
          <Input
            label="Email"
            name="email"
            methods={methods}
            placeholder="Email"
            className="mt-3"
          />
          <Checkbox
            label="Super Admin"
            name="super"
            methods={methods}
            className="mt-3"
          />
          <div className="relative h-14 mt-3">
            <button
              type="submit"
              className="absolute top-6 right-0 bg-green-400 rounded-md px-3 py-1 min-w-[79px]"
            >
              {loading ? (
                <img
                  className="animate-spin h-6 inline-flex justify-center"
                  src={Loading}
                  alt=""
                />
              ) : (
                <div>Simpan</div>
              )}
            </button>
          </div>
        </form>
      </Card>
    </div>
  );
};

export default UpdateAdmin;
