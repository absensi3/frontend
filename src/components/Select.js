import React, { useEffect, useState } from "react";
import useClickOutside from "../utils/clickOutside";

function Select({ methods, label, opts, prefix, suffix, className, ...props }) {
  const [show, setShow] = useState(false);
  const [text, setText] = useState("");
  const [options, setOptions] = useState([]);
  const co = useClickOutside(() => setShow(false));
  useEffect(() => {
    setOptions(opts);
  }, [opts]);
  useEffect(() => {
    setText(
      methods.getValues(props.name)
        ? options.find((o) => o.value === methods.getValues(props.name))
            ?.text ?? ""
        : ""
    );
  }, [methods, options, props.name]);
  return (
    <div className={className}>
      <div className="flex relative">
        {prefix && (
          <div
            className={`flex items-center justify-center border-y-[1px] border-l-[1px] rounded-l-md ${
              methods.formState.errors[props.name]
                ? "border-red-600 fill-red-600"
                : "border-gray-500 fill-black"
            }`}
          >
            {prefix}
          </div>
        )}
        <input
          {...methods.register(props.name)}
          id={props.name}
          name={props.name}
          value={methods.getValues(props.name) ?? ""}
          readOnly
          className="hidden"
        />
        <input
          {...props}
          value={text}
          readOnly
          ref={co}
          className={`border-[1px] peer ${
            methods.formState.errors[props.name]
              ? "border-red-600 text-red-600"
              : "border-gray-500"
          } focus:${
            methods.formState.errors[props.name]
              ? "border-red-600"
              : "border-green-600"
          } focus:outline-none flex-1 px-2 py-1 ${
            prefix ? "" : "rounded-l-md"
          } ${suffix ? "" : "rounded-r-md"}`}
          onFocus={() => setShow(true)}
        />
        <div
          className={`absolute w-full z-50 transition-all duration-300 ${
            show ? "visible opacity-100 top-10" : "invisible opacity-0 top-8"
          }  border-[1px] border-black bg-gray-50 rounded`}
        >
          {options.map((option, idx) => (
            <div
              key={idx}
              value={option.value}
              className="hover:bg-green-300 rounded px-2 py-1"
              onClick={() => {
                methods.setValue(props.name, option.value, {
                  shouldValidate: true,
                  shouldDirty: true,
                });
                setText(option.text);
              }}
            >
              {option.text}
            </div>
          ))}
        </div>
        <label
          htmlFor={props.name}
          className={`absolute peer-placeholder-shown:top-1 transition-all duration-500 peer-placeholder-shown:opacity-0 cursor-text peer-focus:opacity-100 peer-focus:-top-[7px] -top-[7px] peer-focus:text-xs peer-focus:bg-white bg-white ${
            methods.getValues(props.name) && "text-xs"
          } ${
            prefix
              ? "left-[38px] peer-focus:left-[38px] "
              : "left-[9px] peer-focus:left-[9px] "
          } ${methods.formState.errors[props.name] ? "text-red-600" : ""}`}
        >
          {label}
        </label>
        {suffix && (
          <div
            className={`"flex items-center justify-center border-y-[1px] border-r-[1px] rounded-r-md m-1" ${
              methods.formState.errors[props.name]
                ? "border-red-600 fill-red-600"
                : "border-gray-500 fill-black"
            }`}
          >
            {suffix}
          </div>
        )}
      </div>
      {methods.formState.errors[props.name] && (
        <p className="text-red-600 text-xs pl-2">
          {methods.formState.errors[props.name].message}
        </p>
      )}
    </div>
  );
}

export default Select;
