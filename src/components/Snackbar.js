import React, { forwardRef, useImperativeHandle, useState } from "react";
import { ReactComponent as Xmark } from "../assets/xmark.svg";

const Snackbar = forwardRef((props, ref) => {
  const [snackbar, setSnackbar] = useState(false);
  const [msg, setMsg] = useState("");
  const [timeoutID, setTimeoutID] = useState(null);
  const [pos, setPos] = useState("bottom");
  const hideFunc = () => {
    setSnackbar(false);
    clearTimeout(timeoutID);
  };
  useImperativeHandle(ref, () => ({
    show: (message, position = "bottom", duration = 3000) => {
      if (snackbar) {
        clearTimeout(timeoutID);
      }
      setMsg(message);
      setPos(position);
      setSnackbar(true);
      const id = setTimeout(() => {
        setSnackbar(false);
      }, duration);
      setTimeoutID(id);
    },
    hide: hideFunc,
  }));

  return (
    <div
      className={`fixed left-[calc((100vw_-_theme(spacing.80))_/_2)] bg-sky-400 rounded px-4 w-80 py-2 ${
        pos === "bottom" ? "bottom-5" : "top-5"
      } ${snackbar ? "visible" : "invisible"}`}
    >
      <div className="text-justify text-white">{msg}</div>
      <div className="absolute right-1 top-1" onClick={() => hideFunc(false)}>
        <Xmark className="h-4 w-4 fill-gray-200" />
      </div>
    </div>
  );
});

export default Snackbar;
