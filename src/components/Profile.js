import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import React from "react";
import { useForm } from "react-hook-form";
import Card from "./Card";
import Checkbox from "./Checkbox";
import Input from "./Input";
import { useSelector } from "react-redux";
import admin from "../api/admin";

function Profile(props) {
  const profile = useSelector((state) => state.admin.profile);
  const methods = useForm({
    mode: "all",
    defaultValues: {
      username: profile.username,
      email: profile.email,
      super: profile.super,
    },
    resolver: yupResolver(
      yup.object().shape({
        username: yup
          .string()
          .required("Username tidak boleh kosong")
          .min(3, "Username minimal 3 karakter"),
        email: yup
          .string()
          .required("Password tidak boleh kosong")
          .email("Email tidak valid"),
        super: yup.boolean(),
      })
    ),
  });

  const updateProfile = async (data) => {
    try {
      const res = await admin.updateProfile(data);
      if (res.status === 200) {
        props.close();
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Card>
      <form onSubmit={methods.handleSubmit(updateProfile)}>
        <Input label="Username" name="username" methods={methods} />
        <Input
          label="Username"
          name="username"
          methods={methods}
          className="mt-3"
        />
        <Checkbox
          label="Super Admin"
          name="super"
          methods={methods}
          readOnly
          disabled
          className="mt-1"
        />
        <div className="mt-3 flex justify-end gap-3">
          <button
            className="bg-yellow-300 px-2 py-1 rounded"
            onClick={props.close}
          >
            Batal
          </button>
          <button type="submit" className="bg-green-500 px-2 py-1 rounded">
            Simpan
          </button>
        </div>
      </form>
    </Card>
  );
}

export default Profile;
