import React from "react";

function Footer() {
  return (
    <div className="mt-auto bg-sky-700 font-bold h-12 w-full text-center pt-3">
      Footer
    </div>
  );
}

export default Footer;
