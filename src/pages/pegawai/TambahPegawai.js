import { yupResolver } from "@hookform/resolvers/yup";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import Card from "../../components/Card";
import Input from "../../components/Input";
import Loading from "../../assets/loading.png";
import pegawaiApi from "../../api/pegawai";
import Radio from "../../components/Radio";
import DatePicker from "../../components/DatePicker";
import Select from "../../components/Select";
import lembagaApi from "../../api/lembaga";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logoutAction } from "../../store/admin";

const TambahPegawai = ({ showSnackbar }) => {
  const [loading, setLoading] = useState(false);
  const [lembagas, setLembagas] = useState([
    {
      text: "Lembaga 1",
      value: "Value 1",
    },
    {
      text: "Lembaga 2",
      value: "Value 2",
    },
    {
      text: "Lembaga 3",
      value: "Value 3",
    },
  ]);
  const methods = useForm({
    mode: "all",
    resolver: yupResolver(
      yup.object().shape({
        nip: yup
          .string()
          .required("NIP tidak boleh kosong")
          .length(18, "Panjang NIP harus 18 karakter"),
        nama: yup.string().required("Nama tidak boleh kosong"),
        password: yup
          .string()
          .required("Password tidak boleh kosong")
          .min(8, "Password minimal 8 karakter"),
        gender: yup.string().nullable().required("Gender tidak boleh kosong"),
        t_lahir: yup.string().required("Tempat lahir tidak boleh kosong"),
        tgl_lahir: yup.string().required("Tanggal tidak boleh kosong"),
        alamat: yup.string().required("Alamat tidak boleh kosong"),
        no_telp: yup.string().required("No Telpon tidak boleh kosong"),
        email: yup
          .string()
          .required("Password tidak boleh kosong")
          .email("Email tidak valid"),
        satgas: yup.string().required("Satgas tidak boleh kosong"),
        lembaga_id: yup.string().required("Lembaga tidak boleh kosong"),
      })
    ),
  });
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const tambahPegawai = async (data) => {
    setLoading(true);
    try {
      const res = await pegawaiApi.add(data);
      if (res.status === 200) {
        showSnackbar(res.data.msg);
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    } finally {
      methods.reset();
      setLoading(false);
    }
  };
  useEffect(() => {
    const loadLembaga = async () => {
      try {
        const res = await lembagaApi.getAll();
        if (res.status === 200) {
          setLembagas(
            res.data.data.map((lembaga) => {
              return { text: lembaga.nama, value: lembaga.uuid };
            })
          );
        }
      } catch (err) {
        if (err.hasOwnProperty("response")) {
          if (err.response.status === 401) {
            dispatch(logoutAction());
            navigate("/login");
          }
          showSnackbar(err.response.data.msg);
        }
        console.log(err);
      }
    };
    loadLembaga();
  }, [dispatch, navigate, showSnackbar]);

  return (
    <div>
      <Card>
        <div className="text-xl font-bold">Tambah Pegawai</div>
        <form onSubmit={methods.handleSubmit(tambahPegawai)}>
          <Input
            label="NIP"
            name="nip"
            methods={methods}
            placeholder="NIP"
            className="mt-3"
          />
          <Input
            label="Nama"
            name="nama"
            methods={methods}
            placeholder="Nama"
            className="mt-3"
          />
          <Input
            label="Password"
            name="password"
            methods={methods}
            placeholder="Password"
            className="mt-3"
          />
          <Radio
            label="Gender"
            name="gender"
            methods={methods}
            options={[
              {
                label: "Laki-laki",
                value: "L",
              },
              {
                label: "Perempuan",
                value: "P",
              },
            ]}
            className="mt-3"
          />
          <Input
            label="Tempat Lahir"
            name="t_lahir"
            methods={methods}
            placeholder="Tempat Lahir"
            className="mt-3"
          />
          <DatePicker
            label="Tanggal Lahir"
            name="tgl_lahir"
            methods={methods}
            formatText="dd-MM-yyyy"
            placeholder="Tanggal Lahir"
            className="mt-3"
          />
          <Input
            label="Alamat"
            name="alamat"
            methods={methods}
            placeholder="Alamat"
            className="mt-3"
          />
          <Input
            label="No Telp"
            name="no_telp"
            methods={methods}
            placeholder="No Telp"
            className="mt-3"
          />
          <Input
            label="Email"
            name="email"
            methods={methods}
            placeholder="Email"
            className="mt-3"
          />
          <Input
            label="Satgas"
            name="satgas"
            methods={methods}
            placeholder="Satgas"
            className="mt-3"
          />
          <Select
            label="Lembaga"
            name="lembaga_id"
            methods={methods}
            opts={lembagas}
            placeholder="Lembaga"
            className="mt-3"
          />
          {/* <Input
            label="Lembaga"
            name="lembaga"
            methods={methods}
            placeholder="Lembaga"
            className="mt-3"
          /> */}
          <div className="relative h-14 mt-3">
            <button
              type="submit"
              className="absolute top-6 right-0 bg-green-400 rounded-md px-3 py-1 min-w-[79px]"
            >
              {loading ? (
                <img
                  className="animate-spin h-6 inline-flex justify-center"
                  src={Loading}
                  alt=""
                />
              ) : (
                <div>Simpan</div>
              )}
            </button>
          </div>
        </form>
      </Card>
    </div>
  );
};

export default TambahPegawai;
