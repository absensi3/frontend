import { createSlice } from "@reduxjs/toolkit";

export const adminStore = createSlice({
  name: "admin",
  initialState: {
    accessToken: "",
    refreshToken: "",
    profile: {},
  },
  reducers: {
    setAccessToken: (admin, action) => {
      admin.accessToken = action.payload;
    },
    loginAction: (admin, action) => {
      admin.accessToken = action.payload.access_token;
      admin.refreshToken = action.payload.refresh_token;
      admin.profile = action.payload.admin;
    },
    logoutAction: (admin) => {
      admin.accessToken = "";
      admin.refreshToken = "";
      admin.profile = {};
    },
  },
});

export const { loginAction, setAccessToken, logoutAction } = adminStore.actions;

export default adminStore.reducer;
