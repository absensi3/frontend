import api from ".";

const adminApi = {
  login: (data) => {
    return api.post("/auth/admin/login", data);
  },
  logout: () => {
    return api.get("/admin/logout");
  },
  getAll: () => {
    return api.get("/admin");
  },
  getOne: (uuid) => {
    return api.get(`/admin/${uuid}`);
  },
  add: (data) => {
    return api.post("/admin", data);
  },
  updateProfile: (data, uuid) => {
    return api.put(`/admin/${uuid}`, data);
  },
  delete: (uuid) => {
    return api.delete(`admin/${uuid}`);
  },
};

export default adminApi;
