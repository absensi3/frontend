import { yupResolver } from "@hookform/resolvers/yup";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import Card from "../../components/Card";
import Input from "../../components/Input";
import Loading from "../../assets/loading.png";
import lembagaApi from "../../api/lembaga";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { logoutAction } from "../../store/admin";

const TambahLembaga = ({ showSnackbar }) => {
  const [loading, setLoading] = useState(false);
  const methods = useForm({
    mode: "all",
    resolver: yupResolver(
      yup.object().shape({
        nama: yup.string().required("Nama tidak boleh kosong"),
        alamat: yup.string().required("Alamat tidak boleh kosong"),
        lat: yup.number().required("Latitude tidak boleh kosong"),
        long: yup.number().required("Longitude tidak boleh kosong"),
        radius: yup.number().required("Radius tidak boleh kosong"),
      })
    ),
  });
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const tambahLembaga = async (data) => {
    setLoading(true);
    try {
      const res = await lembagaApi.add(data);
      if (res.status === 200) {
        showSnackbar(res.data.msg);
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    } finally {
      methods.reset();
      setLoading(false);
    }
  };
  return (
    <div>
      <Card>
        <div className="text-xl font-bold">Tambah Lembaga</div>
        <form onSubmit={methods.handleSubmit(tambahLembaga)}>
          <Input
            label="Nama"
            name="nama"
            methods={methods}
            placeholder="Nama"
            className="mt-3"
          />
          <Input
            label="Alamat"
            name="alamat"
            methods={methods}
            placeholder="Alamat"
            className="mt-3"
          />
          <Input
            label="Latitude"
            name="lat"
            methods={methods}
            placeholder="Latitude"
            className="mt-3"
          />
          <Input
            label="Longitude"
            name="long"
            methods={methods}
            placeholder="Longitude"
            className="mt-3"
          />
          <Input
            label="Radius"
            name="radius"
            methods={methods}
            placeholder="Radius"
            className="mt-3"
          />
          <div className="relative h-14 mt-3">
            <button
              type="submit"
              className="absolute top-6 right-0 bg-green-400 rounded-md px-3 py-1 min-w-[79px]"
            >
              {loading ? (
                <img
                  className="animate-spin h-6 inline-flex justify-center"
                  src={Loading}
                  alt=""
                />
              ) : (
                <div>Simpan</div>
              )}
            </button>
          </div>
        </form>
      </Card>
    </div>
  );
};

export default TambahLembaga;
