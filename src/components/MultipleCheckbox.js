import React from "react";
import Checkbox from "./Checkbox";

function MultipleCheckbox({ methods, label, className, options, ...props }) {
  return (
    <div className={className}>
      <label>{label}</label>
      <div
        className={`grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 border-[1px] border-gray-500 rounded p-3 ${
          methods.formState.errors[props.name]
            ? "border-red-600"
            : "border-gray-500"
        }`}
      >
        {options.map((option, idx) => (
          <Checkbox
            key={idx}
            className={
              methods.formState.errors[props.name]
                ? "text-red-600"
                : "text-gray-500"
            }
            register={methods.register}
            label={option.label}
            trueValue={option.value}
          />
        ))}
      </div>
      {methods.formState.errors[props.name] && (
        <p className="text-red-600 text-xs pl-2">
          {methods.formState.errors[props.name].message}
        </p>
      )}
    </div>
  );
}

export default MultipleCheckbox;
