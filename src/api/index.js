import axios from "axios";
import { store } from "../store";
import { setAccessToken } from "../store/admin";

const api = axios.create({
  baseURL: "http://localhost:8000",
});

const localRefresh = () => {
  return axios.get(`${api.getUri()}/auth/admin/refresh`, {
    headers: { Authorization: `Bearer ${store.getState().admin.refreshToken}` },
  });
};

api.interceptors.request.use((req) => {
  if (!req.url.includes("/login")) {
    req.headers = {
      Authorization: `Bearer ${store.getState().admin.accessToken}`,
    };
  }
  return req;
});

api.interceptors.response.use(
  async (res) => {
    return res;
  },
  async (err) => {
    const str = store.getState();
    const originalRequest = err.config;
    if (
      err.response.status === 401 &&
      !originalRequest._retry &&
      str.admin.refreshToken &&
      !originalRequest.url.includes("logout")
    ) {
      originalRequest._retry = true;
      try {
        let res = await localRefresh();
        if (res.status === 200) {
          store.dispatch(setAccessToken(res.data.data));
          originalRequest.headers.Authorization = `Bearer ${res.data.data.access_token}`;
          return api(originalRequest);
        }
      } catch (error) {
        return Promise.reject(err);
      }
    } else return Promise.reject(err);
  }
);

export default api;
