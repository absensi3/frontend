import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import BurgerButton from "../components/BurgerButton";
import Dropdown from "../components/Dropdown";
import DropdownItem from "../components/DropdownItem";
import { ReactComponent as UserIcon } from "../assets/user-2.svg";
import admin from "../api/admin";
import { logoutAction } from "../store/admin";
import { useNavigate } from "react-router-dom";
import Profile from "../components/Profile";
import Dialog from "../components/Dialog";

function Header(props) {
  const profile = useSelector((state) => state.admin.profile);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [profileDialog, setProfileDialog] = useState(false);

  const logout = async () => {
    try {
      const res = await admin.logout();
      if (res.status === 200) {
        dispatch(logoutAction());
        navigate("/login");
      }
    } catch (err) {
      console.log(err);
      dispatch(logoutAction());
      navigate("/login");
    }
  };
  return (
    <div className="sticky top-0 bg-sky-700 w-full z-10 flex justify-between">
      <div className="flex items-center">
        <BurgerButton open={props.showSidebar} onClick={props.toggleSidebar} />
        <div className="font-semibold text-white">Absensi</div>
      </div>
      <div className="flex items-center px-3">
        <Dropdown
          toggle={
            <div className="-mx-2 md:mx-3 my-auto flex">
              <span className="my-auto font-semibold px-2 text-white">
                {profile.username}
              </span>
              {/* <img src={profileUser} alt="" className="h-6 w-6" /> */}
              <UserIcon className="h-6 w-6 p-1 rounded-full bg-black fill-white" />
            </div>
          }
        >
          <DropdownItem
            onClick={() => setProfileDialog(true)}
            className="w-48 font-semibold hover:bg-green-300 hover:rounded-md text-left mx-[1px] my-[1px] px-2 md:py-1"
          >
            Profile
          </DropdownItem>
          <DropdownItem className="font-semibold hover:bg-green-300 hover:rounded-md text-left mx-[1px] my-[1px] px-2 md:py-1">
            Update Password
          </DropdownItem>
          <DropdownItem
            onClick={logout}
            className="font-semibold hover:bg-green-300 hover:rounded-md text-left mx-[1px] my-[1px] px-2 md:py-1"
          >
            Logout
          </DropdownItem>
        </Dropdown>
      </div>
      <Dialog show={profileDialog} toggle={setProfileDialog}>
        <Profile />
      </Dialog>
    </div>
  );
}

export default Header;
