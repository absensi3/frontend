import { configureStore } from "@reduxjs/toolkit";
import adminStore from "./admin";
import storage from "redux-persist/lib/storage";
import { persistReducer, persistStore } from "redux-persist";
import thunk from "redux-thunk";

export const store = configureStore({
  reducer: {
    admin: persistReducer(
      {
        key: "kominfo",
        storage,
      },
      adminStore
    ),
  },
  middleware: [thunk],
});

export const persistor = persistStore(store);
