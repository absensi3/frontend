import api from ".";

const pegawaiApi = {
  getAll: () => {
    return api.get(`/pegawai`);
  },
  add: (data) => {
    return api.post(`/pegawai`, data);
  },
  getOne: (pegawaiId) => {
    return api.get(`/pegawai/${pegawaiId}`);
  },
  update: (data, pegawaiId) => {
    return api.put(`/pegawai/${pegawaiId}`, data);
  },
  delete: (pegawaiId) => {
    return api.delete(`/pegawai/${pegawaiId}`);
  },
};

export default pegawaiApi;
