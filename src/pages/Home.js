import React from "react";
import { useSelector } from "react-redux";
import logo from "../assets/logo.svg";

function Home() {
  const token = useSelector((state) => state.admin.accessToken);
  console.log(token);
  return (
    <div className="">
      <img alt="" src={logo} />
    </div>
  );
}

export default Home;
