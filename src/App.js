import { useSelector } from "react-redux";
import { Navigate, Outlet, Route, Routes } from "react-router-dom";
import Main from "./layout/Main";
import DaftarAdmin from "./pages/admin/DaftarAdmin";
import DaftarPegawai from "./pages/pegawai/DaftarPegawai";
// import Home from "./pages/Home";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import TambahAdmin from "./pages/admin/TambahAdmin";
import TambahPegawai from "./pages/pegawai/TambahPegawai";
import UpdateAdmin from "./pages/admin/UpdateAdmin";
import UpdatePegawai from "./pages/pegawai/UpdatePegawai";
import DaftarLembaga from "./pages/lembaga/DaftarLembaga";
import TambahLembaga from "./pages/lembaga/TambahLembaga";
import UpdateLembaga from "./pages/lembaga/UpdateLembaga";
import { useRef } from "react";

const App = () => {
  const profile = useSelector((state) => state.admin.profile);
  // const [snackbar, setSnackbar] = useState(false);
  // const [msg, setMsg] = useState("");
  // const [call, setCall] = useState(0);
  const mainRef = useRef();
  const showSnackbar = (msg) => {
    mainRef.current.showSnackbar(msg);
    // setMsg(msg);
    // setSnackbar(true);
    // setCall(call + 1);
  };
  return (
    <Routes>
      <Route
        path="/"
        element={
          Object.keys(profile).length === 0 ? (
            <Navigate to="/login" replace />
          ) : (
            <div className="bg-gray-100 min-h-screen text-base">
              <Outlet />
            </div>
          )
        }
      >
        <Route
          path=""
          element={
            Object.keys(profile).length === 0 ? (
              <Navigate to="/login" replace />
            ) : (
              // <Main snackbar={{ show: snackbar, msg: msg, call: call }} />
              <Main ref={mainRef} />
            )
          }
        >
          <Route
            index
            path=""
            // element={<DaftarAdmin showSnackbar={showSnackbar} />}
            element={<DaftarAdmin showSnackbar={showSnackbar} />}
          />
          <Route
            path="tambah-admin"
            // element={<TambahAdmin showSnackbar={showSnackbar} />}
            element={<TambahAdmin showSnackbar={showSnackbar} />}
          />
          <Route
            path="update-admin/:uuid"
            // element={<UpdateAdmin showSnackbar={showSnackbar} />}
            element={<UpdateAdmin showSnackbar={showSnackbar} />}
          />
          <Route
            path="pegawai"
            // element={<DaftarPegawai showSnackbar={showSnackbar} />}
            element={<DaftarPegawai showSnackbar={showSnackbar} />}
          />
          <Route
            path="tambah-pegawai"
            // element={<TambahPegawai showSnackbar={showSnackbar} />}
            element={<TambahPegawai showSnackbar={showSnackbar} />}
          />
          <Route
            path="update-pegawai/:uuid"
            // element={<UpdatePegawai showSnackbar={showSnackbar} />}
            element={<UpdatePegawai showSnackbar={showSnackbar} />}
          />
          <Route
            path="lembaga"
            // element={<DaftarLembaga showSnackbar={showSnackbar} />}
            element={<DaftarLembaga showSnackbar={showSnackbar} />}
          />
          <Route
            path="tambah-lembaga"
            // element={<TambahLembaga showSnackbar={showSnackbar} />}
            element={<TambahLembaga showSnackbar={showSnackbar} />}
          />
          <Route
            path="update-lembaga/:uuid"
            // element={<UpdateLembaga showSnackbar={showSnackbar} />}
            element={<UpdateLembaga showSnackbar={showSnackbar} />}
          />
        </Route>
      </Route>
      <Route
        path="/login"
        element={
          Object.keys(profile).length === 0 ? (
            <Login />
          ) : (
            <Navigate to="/" replace />
          )
        }
      />
      <Route path="*" element={<NotFound />} />
    </Routes>
  );
};

export default App;
