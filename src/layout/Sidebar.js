import React from "react";
import { Link, useLocation } from "react-router-dom";
import logo from "../assets/react.png";
import nav from "./navigator";

function Sidebar(props) {
  const currentRoute = useLocation();
  return (
    <div
      className={`bg-sky-200 transition-all duration-500 z-20 lg:z-0 
      ${props.show ? "w-60" : "w-0"}`}
    >
      <img className="p-3" src={logo} alt="" />
      <div className="h-[0.5px] bg-sky-700 w-full"></div>
      <div
        className={`transition-all duration-200 ${
          props.show ? "opacity-100" : "opacity-0"
        }`}
      >
        {nav.map((n, idx) => (
          <Link key={idx} to={n.path}>
            <div
              className={`font-semibold w-full rounded hover:bg-sky-500 p-3 my-3 ${
                currentRoute.pathname === n.path ? "bg-sky-300" : ""
              }`}
            >
              {n.title}
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
}

export default Sidebar;
