const nav = [
  {
    path: "/",
    title: "Daftar Admin",
  },
  {
    path: "/tambah-admin",
    title: "Tambah Admin",
  },
  {
    path: "/pegawai",
    title: "Daftar Pegawai",
  },
  {
    path: "/tambah-pegawai",
    title: "Tambah Pegawai",
  },
  {
    path: "/lembaga",
    title: "Daftar Lembaga",
  },
  {
    path: "/tambah-lembaga",
    title: "Tambah Lembaga",
  },
];

export default nav;
