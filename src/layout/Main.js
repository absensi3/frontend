import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  useState,
} from "react";
import { Outlet } from "react-router-dom";
import Snackbar from "../components/Snackbar";
import Footer from "./Footer";
import Header from "./Header";
import Sidebar from "./Sidebar";

const Main = forwardRef((props, ref) => {
  const [show, setShow] = useState(false);
  // const [snackbar, setSnackbar] = useState(false);
  const snackbarRef = useRef();
  // const [msg, setMsg] = useState("");
  useImperativeHandle(ref, () => ({
    showSnackbar: (message) => {
      snackbarRef.current.show(message);
    },
  }));
  // useEffect(() => {
  //   const showSnackbar = (msg) => {
  //     setSnackbar(false);
  //     setMsg(msg);
  //     setTimeout(() => setSnackbar(true));
  //   };
  //   if (props.snackbar.show) showSnackbar(props.snackbar.msg);
  // }, [props.snackbar]);
  return (
    <div className="flex  min-h-screen">
      <Sidebar show={show} />
      <div
        className={`h-screen flex-1 min-w-screen overflow-auto absolute w-full lg:relative transition duration-500 ease-in-out`}
      >
        <Header showSidebar={show} toggleSidebar={() => setShow(!show)} />
        <main
          className={`p-3 bg-gray-300 mx-auto min-h-[calc(100vh_-_theme(spacing.12)_-_theme(spacing.12))]`}
        >
          <Outlet />
        </main>
        <Footer />
      </div>
      <div
        className={`fixed inset-0 opacity-30 bg-black  transition-all duration-300 z-10 lg:hidden ${
          show ? "" : "hidden"
        }`}
        onClick={() => setShow(!show)}
      ></div>
      <Snackbar ref={snackbarRef} />
    </div>
  );
});

// function Main(props) {
//   const [show, setShow] = useState(false);
//   const [snackbar, setSnackbar] = useState(false);
//   const snackbarRef = useRef()
//   const [msg, setMsg] = useState("");
//   useEffect(() => {
//     const showSnackbar = (msg) => {
//       setSnackbar(false);
//       setMsg(msg);
//       setTimeout(() => setSnackbar(true));
//     };
//     if (props.snackbar.show) showSnackbar(props.snackbar.msg);
//   }, [props.snackbar]);
//   return (
//     <div className="flex  min-h-screen">
//       <Sidebar show={show} />
//       <div
//         className={`h-screen flex-1 min-w-screen overflow-auto absolute w-full lg:relative transition duration-500 ease-in-out`}
//       >
//         <Header showSidebar={show} toggleSidebar={() => setShow(!show)} />
//         <main
//           className={`p-3 bg-gray-300 mx-auto min-h-[calc(100vh_-_theme(spacing.12)_-_theme(spacing.12))]`}
//         >
//           <Outlet />
//         </main>
//         <Footer />
//       </div>
//       <div
//         className={`fixed inset-0 opacity-30 bg-black  transition-all duration-300 z-10 lg:hidden ${
//           show ? "" : "hidden"
//         }`}
//         onClick={() => setShow(!show)}
//       ></div>
//       <Snackbar ref={snackbarRef} />
//     </div>
//   );
// }

export default Main;
