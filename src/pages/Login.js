import React, { useRef, useState } from "react";
import Card from "../components/Card";
import Input from "../components/Input";
import { ReactComponent as View } from "../assets/eye.svg";
import { ReactComponent as Hidden } from "../assets/eye-off.svg";
import { ReactComponent as UserIcon } from "../assets/user-2.svg";
import Checkbox from "../components/Checkbox";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import adminApi from "../api/admin";
import { useDispatch } from "react-redux";
import { loginAction } from "../store/admin";
import { useNavigate } from "react-router-dom";
import Snackbar from "../components/Snackbar";

function Login() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const snackbarRef = useRef();
  const methods = useForm({
    mode: "all",
    resolver: yupResolver(
      yup.object().shape({
        username: yup
          .string()
          .required("Username tidak boleh kosong")
          .min(3, "Username minimal 3 karakter"),
        password: yup.string().required("Password tidak boleh kosong").min(8),
        remember_me: yup.boolean(),
      })
    ),
  });

  const [showPassword, setShowPassword] = useState(false);

  const login = async (data) => {
    try {
      const res = await adminApi.login(data);
      if (res.status === 200) {
        dispatch(loginAction(res.data.data));
        navigate("/admin");
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        snackbarRef.current.show(err.response.data.msg);
      }
    }
  };

  return (
    <div>
      <div className="box-border h-24"></div>
      <div className="flex justify-center items-center">
        <Card className="w-80">
          <div className="flex flex-col items-center">
            <UserIcon className="h-24 w-24 bg-black fill-white rounded-full p-3" />
            <div className="text-lg font-bold">Login</div>
          </div>
          <form onSubmit={methods.handleSubmit(login)}>
            <Input
              label="Username"
              name="username"
              methods={methods}
              placeholder="Your Username"
              className="mt-3"
            />
            <Input
              label="Password"
              name="password"
              methods={methods}
              placeholder="Your password"
              type={showPassword ? "text" : "password"}
              className="mt-3"
              suffix={
                <button
                  type="button"
                  className="h-full px-1 outline-none focus:outline-none"
                  onClick={() => setShowPassword(!showPassword)}
                >
                  <View
                    className={`h-6 w-6 fill-inherit ${
                      showPassword ? "hidden" : "block"
                    }`}
                  />
                  <Hidden
                    className={`h-6 w-6 fill-inherit ${
                      showPassword ? "block" : "hidden"
                    }`}
                  />
                </button>
              }
            />
            <Checkbox label="Ingat saya" name="remember_me" methods={methods} />
            <div className="mt-5">
              <button
                className="w-full bg-green-500 py-1 rounded text-white hover:bg-green-600"
                type="submit"
              >
                Login
              </button>
            </div>
          </form>
        </Card>
      </div>
      <Snackbar ref={snackbarRef} />
    </div>
  );
}

export default Login;
