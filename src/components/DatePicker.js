import {
  addDays,
  eachWeekOfInterval,
  format,
  isSameMonth,
  isToday,
  parse,
  isSameDay,
} from "date-fns";
import React, { useEffect, useState } from "react";
import useClickOutside from "../utils/clickOutside";

function DatePicker({
  methods,
  label,
  formatText = "yyyy-MM-dd",
  formatValue = "yyyy-MM-dd",
  prefix,
  suffix,
  className,
  ...props
}) {
  const [show, setShow] = useState(false);
  const now = new Date();
  const [selected, setSelected] = useState(now);
  const [formatedValue, setFormatedValue] = useState("");
  const [formatedText, setFormatedText] = useState("");
  const [year, setYear] = useState(now.getFullYear());
  const [month, setMonth] = useState(now.getMonth());
  const [weeks, setWeeks] = useState([]);
  useEffect(() => {
    setWeeks(
      eachWeekOfInterval({
        start: new Date(year, month, 1),
        end: addDays(new Date(year, month, 1), 30),
      })
    );
  }, [month, year]);
  useEffect(() => {
    const subs = methods.watch((val) => {
      setSelected(parse(val[props.name], formatValue, new Date()));
      setFormatedValue(val[props.name] ?? "");
      setFormatedText(
        val[props.name]
          ? format(parse(val[props.name], formatValue, new Date()), formatText)
          : ""
      );
    });
    return () => {
      subs.unsubscribe();
    };
  }, [formatText, formatValue, methods, props.name]);
  const co = useClickOutside(() => {
    setShow(false);
  });
  return (
    <div className={className}>
      <div className="flex relative">
        {prefix && (
          <div
            className={`flex items-center justify-center border-y-[1px] border-l-[1px] rounded-l-md ${
              methods.formState.errors[props.name]
                ? "border-red-600 fill-red-600"
                : "border-gray-500 fill-black"
            }`}
          >
            {prefix}
          </div>
        )}
        <input
          {...methods.register(props.name)}
          id={props.name}
          name={props.name}
          className="hidden"
          value={formatedValue}
          readOnly
        />
        <input
          {...props}
          ref={co}
          value={formatedText}
          className={`border-[1px] peer ${
            methods.formState.errors[props.name]
              ? "border-red-600 text-red-600"
              : "border-gray-500"
          } focus:${
            methods.formState.errors[props.name]
              ? "border-red-600"
              : "border-green-600"
          } focus:outline-none flex-1 px-2 py-1 ${
            prefix ? "" : "rounded-l-md"
          } ${suffix ? "" : "rounded-r-md"}`}
          onFocus={() => setShow(true)}
          readOnly
        />
        <div
          className={`absolute w-64 z-50 flex flex-col items-center justify-center transition-all duration-300 ${
            show ? "visible opacity-100 top-10" : "invisible opacity-0 top-8"
          }  border-[1px] border-black bg-gray-50 rounded`}
          onClick={(e) => e.stopPropagation()}
        >
          <div>{format(selected ? selected : now, "d MMMM yyyy")}</div>
          <div className="flex flex-row justify-around w-full">
            <div onClick={() => setYear(year - 1)}>{"<"}</div>
            <div className="w-24 inline-flex justify-center">
              {format(new Date(year, month), "yyyy")}
            </div>
            <div onClick={() => setYear(year + 1)}>{">"}</div>
          </div>
          <div className="flex flex-row justify-around w-full">
            <div onClick={() => setMonth(month - 1)}>{"<"}</div>
            <div className="w-24 inline-flex justify-center">
              {format(new Date(year, month), "MMMM")}
            </div>
            <div onClick={() => setMonth(month + 1)}>{">"}</div>
          </div>
          <div className="flex flex-row justify-evenly w-full">
            <div className="text-red-500">A</div>
            <div>S</div>
            <div>S</div>
            <div>R</div>
            <div>K</div>
            <div className="text-green-500">J</div>
            <div>S</div>
          </div>
          {weeks.map((week, idx) => (
            <div key={idx} className="flex flex-row justify-evenly w-full px-2">
              {[0, 1, 2, 3, 4, 5, 6].map((e, idx) => (
                <div
                  key={idx}
                  className={`w-3 rounded-full hover:bg-green-300 px-3 inline-flex justify-center ${
                    isSameMonth(addDays(week, e), new Date(year, month))
                      ? e === 0
                        ? "text-red-500"
                        : e === 5
                        ? "text-green-500"
                        : "text-black"
                      : "text-gray-300"
                  } ${
                    isToday(addDays(week, e)) ? "border border-gray-500" : ""
                  } ${
                    isSameDay(selected, addDays(week, e)) ? "bg-green-200" : ""
                  }`}
                  onClick={() => {
                    const selectedValue = addDays(week, e);
                    const selectedFormat = format(selectedValue, formatValue);
                    setSelected(selectedValue);
                    setFormatedValue(selectedFormat);
                    methods.setValue(props.name, selectedFormat, {
                      shouldValidate: true,
                      shouldDirty: true,
                    });
                    setFormatedText(format(selectedValue, formatText));
                    setShow(false);
                  }}
                >
                  {format(addDays(week, e), "d")}
                </div>
              ))}
            </div>
          ))}
        </div>
        <label
          htmlFor={props.name}
          className={`absolute peer-placeholder-shown:top-1 transition-all duration-500 peer-placeholder-shown:opacity-0 cursor-text peer-focus:opacity-100 peer-focus:-top-[7px] -top-[7px] peer-focus:text-xs peer-focus:bg-white bg-white ${
            methods.getValues(props.name) && "text-xs"
          } ${
            prefix
              ? "left-[38px] peer-focus:left-[38px] "
              : "left-[9px] peer-focus:left-[9px] "
          } ${methods.formState.errors[props.name] ? "text-red-600" : ""}`}
        >
          {label}
        </label>
        {suffix && (
          <div
            className={`"flex items-center justify-center border-y-[1px] border-r-[1px] rounded-r-md m-1" ${
              methods.formState.errors[props.name]
                ? "border-red-600 fill-red-600"
                : "border-gray-500 fill-black"
            }`}
          >
            {suffix}
          </div>
        )}
      </div>
      {methods.formState.errors[props.name] && (
        <p className="text-red-600 text-xs pl-2">
          {methods.formState.errors[props.name].message}
        </p>
      )}
    </div>
  );
}

export default DatePicker;
