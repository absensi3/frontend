import React, { useState } from "react";

function Dropdown(props) {
  const [show, setShow] = useState(false);
  return (
    <div className="relative">
      <button onClick={() => setShow(!show)} onBlur={() => setShow(false)}>
        {props.toggle}
      </button>
      <div
        className={`absolute transition-all flex flex-col p-1 gap-1 duration-300 bg-white rounded-lg shadow-md top-full mt-2 right-0 ${
          show ? "" : "invisible opacity-0 -translate-y-2"
        }`}
      >
        {props.children}
      </div>
    </div>
  );
}

export default Dropdown;
