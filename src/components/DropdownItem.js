import React from "react";

function DropdownItem(props) {
  return (
    <button
      {...props}
      className={
        props.className
          ? props.className
          : "hover:bg-gray-300 rounded-md transition-colors duration-150"
      }
    >
      {props.children}
    </button>
  );
}

export default DropdownItem;
