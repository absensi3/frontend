import React, { useCallback, useEffect, useState } from "react";
import pegawaiApi from "../../api/pegawai";
import Card from "../../components/Card";
import { ReactComponent as Pen } from "../../assets/pen.svg";
import { ReactComponent as Trush } from "../../assets/trash.svg";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logoutAction } from "../../store/admin";
import Dialog from "../../components/Dialog";

function DaftarPegawai({ showSnackbar }) {
  const [pegawais, setPegawais] = useState([]);
  const [deleteDialog, setDeleteDialog] = useState(false);
  const [selected, setSelected] = useState("");
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const deletePegawai = async () => {
    try {
      console.log(selected);
      const res = await pegawaiApi.delete(selected);
      if (res.status === 200) {
        showSnackbar(res.data.msg);
        setSelected("");
        setDeleteDialog(false);
        loadPegawai();
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    }
  };
  const loadPegawai = useCallback(async () => {
    try {
      const res = await pegawaiApi.getAll();
      if (res.status === 200) {
        setPegawais(res.data.data);
      }
    } catch (err) {
      if (err.hasOwnProperty("response")) {
        if (err.response.status === 401) {
          dispatch(logoutAction());
          navigate("/login");
        }
        showSnackbar(err.response.data.msg);
      }
      console.log(err);
    }
  }, [dispatch, navigate, showSnackbar]);

  useEffect(() => {
    loadPegawai();
  }, [loadPegawai]);

  return (
    <div>
      <Card>
        <div className="text-lg font-semibold">Daftar Pegawai</div>
        <div className="overflow-x-auto">
          <table className="overflow-scroll rounded shadow w-full mt-3">
            <thead>
              <tr className="bg-gray-200 text-left">
                <th className="px-2 py-2 w-44 rounded-tl">NIP</th>
                <th className="px-2 py-2">Nama</th>
                <th className="px-2 py-2 w-32">No Telp</th>
                <th className="px-2 py-2">Email</th>
                <th className="px-2 py-2 w-24">Satgas</th>
                <th className="px-2 py-2 w-24 rounded-tr">Aksi</th>
              </tr>
            </thead>
            <tbody>
              {pegawais.map((pegawai, idx) => (
                <tr
                  key={idx}
                  className="hover:bg-gray-100 odd:bg-gray-50 even:bg-gray-200"
                >
                  <td className="px-2 py-2">{pegawai.nip}</td>
                  <td className="px-2 py-2 w-52">{pegawai.nama}</td>
                  <td className="px-2 py-2">{pegawai.no_telp}</td>
                  <td className="px-2 py-2">{pegawai.email}</td>
                  <td className="px-2 py-2">{pegawai.satgas}</td>
                  <td className="px-2 py-2 flex gap-2">
                    <button
                      className="rounded-full bg-green-400 p-2"
                      onClick={() => {
                        setSelected(pegawai.uuid);
                        setDeleteDialog(true);
                      }}
                    >
                      <Trush className="h-5 w-5  fill-gray-200" />
                    </button>
                    <button
                      onClick={() =>
                        navigate(`/update-pegawai/${pegawai.uuid}`)
                      }
                      className="rounded-full bg-green-400 p-2"
                    >
                      <Pen className="h-5 w-5  fill-gray-200" />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Card>
      <Dialog show={deleteDialog} toggle={() => setDeleteDialog(false)}>
        <Card>
          <div>{`Apakah anda yakin untuk menghapus pegawai ${
            selected &&
            pegawais.find((pegawai) => pegawai.uuid === selected).nama
          }`}</div>
          <div className="flex justify-end mt-5">
            <button
              className="bg-red-600 px-4 py-1 rounded text-white"
              onClick={() => deletePegawai()}
            >
              Ya
            </button>
            <button
              className="ml-5 bg-green-500 px-4 py-1 rounded"
              onClick={() => setDeleteDialog(false)}
            >
              Tidak
            </button>
          </div>
        </Card>
      </Dialog>
    </div>
  );
}

export default DaftarPegawai;
